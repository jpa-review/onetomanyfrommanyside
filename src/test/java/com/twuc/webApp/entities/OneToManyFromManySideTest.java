package com.twuc.webApp.entities;

import com.twuc.webApp.repositories.StudentRepository;
import com.twuc.webApp.repositories.TeacherRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest(showSql = false)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class OneToManyFromManySideTest {
    @Autowired
    TeacherRepository teacherRepository;

    @Autowired
    StudentRepository studentRepository;

    @Autowired
    EntityManager entityManager;

    @Test
    void should_save_teacher_from_many_side() {
        Student student = new Student("陈奕迅");
        Student anotherStudent = new Student("林俊杰");

        Teacher teacher = new Teacher("李嘉豪");

        student.setTeacher(teacher);
        anotherStudent.setTeacher(teacher);

        studentRepository.save(student);
        studentRepository.save(anotherStudent);
        studentRepository.flush();

        Teacher find = teacherRepository.findById(1L).orElseThrow(RuntimeException::new);
        int studentsNumber = studentRepository.findAll().size();

        assertEquals("李嘉豪", find.getName());
        assertEquals(2, studentsNumber);
    }
}